#!/usr/bin/python
#getting: time|user|content

import urllib
import json, difflib
from csv import writer


differ = difflib.Differ()

pagetitle = 'War_on_Terror'
#baseq = 'http://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvlimit=500&titles=War_on_Terror&rvprop=timestamp|user|comment|ids&format=json'
baseq = 'http://en.wikipedia.org/w/api.php?action=query&prop=revisions&rvlimit=5&titles='+pagetitle+'&rvprop=timestamp|user|comment|content|ids&format=json'
q=baseq
count = 0
csvfile = open('revisions.csv', 'w')
w = writer(csvfile)
w.writerow(("revid", "timestamp", "user", "comment", "content"))
lastContent = []

while True:
  results = json.load(urllib.urlopen(q))
  
  p = results['query']['pages']
  for key in p:
    pass
  revs = p[key]['revisions']
  count += len(revs)
  print revs[-1]['timestamp']
  print len(revs)
  
  for r in revs:
    print "*"*80
    print r['timestamp']
    content = r['*'].splitlines()
    for d in differ.compare(lastContent, content):
      if d.startswith("+"):
        # print d
        w.writerow((r['revid'], r['timestamp'], r['user'].encode('utf-8'), r['comment'].encode('utf-8'), d.encode('utf-8')))
    lastContent = content

  rvcontinue = None
  if 'query-continue' in results:
    if 'revisions' in results['query-continue']:
      if 'rvcontinue' in results['query-continue']['revisions']:
        rvcontinue = results['query-continue']['revisions']['rvcontinue']
        q = baseq+"&rvcontinue="+str(rvcontinue)
  if rvcontinue==None:
    break
  break

print "done"
print count, "total revs"
csvfile.close()


