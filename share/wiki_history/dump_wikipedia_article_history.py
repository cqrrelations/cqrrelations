#!/usr/bin/python
#getting: time|user|content

from urllib import quote
from urllib2 import urlopen
import json, difflib
import argparse, sys

parser = argparse.ArgumentParser(description='Dump the history of a wikipedia article as JSON')
parser.add_argument('title', help='The title of the article')
parser.add_argument('--language', default="en", help='Language code of the article (default: en)')
parser.add_argument('--limit', type=int, default=None, help='How many revisions are retrieved (default: all)')
parser.add_argument('--rvlimit', type=int, default=50, help='Advanced: rvlimit controls how many revisions are retrieved at a time from the API.')

args = parser.parse_args()
pagetitle = quote(args.title.replace(" ", "_"))
rvlimit = args.rvlimit
limit = args.limit
if limit != None:
  rvlimit = min(limit, args.rvlimit)

baseq = 'http://'+args.language+'.wikipedia.org/w/api.php?action=query&prop=revisions&rvlimit='+str(rvlimit)+'&titles='+pagetitle+'&rvprop=timestamp|user|comment|content|ids|tags&format=json'
q=baseq
count = 0

data = {}
revisions = []
data['revisions'] = revisions
lastContent = []
differ = difflib.Differ()

while True:
  print >> sys.stderr, q
  results = json.load(urlopen(q))  
  p = results['query']['pages']
  for key in p:
    pass
  revs = p[key]['revisions']
  print >> sys.stderr, revs[-1]['timestamp']
  
  for r in revs:
    revision = {
      'revid': r['revid'],
      'timestamp': r['timestamp'],
      'user': r['user'],
      'tags': r['tags'],
      'comment': r.get('comment', '').encode("utf-8"),
      'content_diff': []
    }
    rc = revision['content_diff']
    content = r['*'].splitlines()
    for line in list(difflib.unified_diff(lastContent, content))[2:]:
      rc.append(line)
    count +=1
    revisions.append(revision)
    lastContent = content

  if limit != None and count >= limit:
    break

  rvcontinue = None
  if 'query-continue' in results:
    if 'revisions' in results['query-continue']:
      if 'rvcontinue' in results['query-continue']['revisions']:
        rvcontinue = results['query-continue']['revisions']['rvcontinue']
        q = baseq+"&rvcontinue="+str(rvcontinue)

  if rvcontinue==None:
    break

print >> sys.stderr, count, "total revisions"
print json.dumps(data)



