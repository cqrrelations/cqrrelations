##library � charger
library(bitops)
library(gdata)
library(caTools)
library(tools)
library(gplots)
library(timeDate)
library(timeSeries)
library(fBasics)
library(igraph)
library(tcltk )
library(ade4)
library(Cairo)
library(igraph)
##base Folder ..
setwd('C:\\Users\\sony\\Desktop\\patate' )


##analyse of the pedigree (BD of 5600 potatoes, extraction of the pedigree
Read the DB
C=read.csv("ped4.csv",sep=';',header=TRUE)
#Coerce the format as a matrix
C2=as.matrix(C)
#Coerce the format to a graph
G=graph.edgelist(C2, directed=TRUE)

#Graphs
tkplot(G,canvas.width=800, canvas.height=600,edge.color="red",vertex.size=1,vertex.color="Blue",
vertex.label.color="Navy Blue",edge.curved=TRUE, edge.arrow.size=0.6)


#Communities 
wc <- walktrap.community(G)
plot(wc, G)
dendPlot(wc)








##Graph of kate file
C=read.csv("exportmatrix.csv",sep=';',header=TRUE)
 a=dim(C)          
    a
 #forcer le statut matrice
Mat=data.matrix(C[,2:15], rownames.force = T)     
row.names(Mat)<-C[,1]   
  a
 #produit matriciel
 AdjMat= Mat%*%t(Mat)
 Mat%*%t(Mat)
 
F=graph.adjacency(AdjMat,diag=FALSE,weighted=TRUE,mode="undirected")
names.Vertex=rownames(AdjMat)  
                
plot.igraph(F,layout=layout.fruchterman.reingold,
edge.width =( E(F)$weight)*( E(F)$weight),
vertex.size=100,vertex.label=names.Vertex, edge.color="black",edge.curved=FALSE, edge.arrow.size=0)

##Chargement de la table des gouts...
setwd('C:\\Users\\sony\\Desktop\\patate' )


##premi�re partie :     the steps
 C=read.csv("exportmatrix.csv",sep=';',header=TRUE)
 a=dim(C)          
    a
 #forcer le statut matrice
Mat=data.matrix(C[,2:15], rownames.force = T)     
row.names(Mat)<-C[,1]   
  a
 #produit matriciel
 AdjMat= Mat%*%t(Mat)
 Mat%*%t(Mat)
 
F=graph.adjacency(AdjMat,diag=FALSE,weighted=TRUE,mode="undirected")
names.Vertex=rownames(AdjMat)  
                
plot.igraph(F,layout=layout.fruchterman.reingold,vertex.size=10,vertex.label=names.Vertex)
           clusters(F)
           
   graph.adjacency( AdjMat)

plot.igraph(F,layout=layout.fruchterman.reingold,
edge.width =( E(F)$weight)*( E(F)$weight),
vertex.size=100,vertex.label=names.Vertex, edge.color="black",edge.curved=FALSE, edge.arrow.size=0)

  
  
Fconnect=graph.adjacency(AdjMat,diag=FALSE,weighted=TRUE,mode="undirected")
names.Vertex.Connect=substring(rownames(AdjMat),1,15)

                                       
plot.igraph(Fconnect,layout=layout.reingold.tilford,vertex.size=1,vertex.label=names.Vertex.Connect)

  
                                                  

tkplot(F, canvas.width=800, canvas.height=700,vertex.label=names.Vertex,
edge.width =( E(F)$weight/200),edge.color="darkgreen",vertex.size=7,vertex.color="yellow",vertex.label.color="black",edge.curved=FALSE, edge.arrow.size=0,tkplot.fit.to.screen)





###deuxi�me partie les d�parts / arriv�es
C1=read.csv("exportmatrix2.csv",sep=';',header=TRUE)
 C2=C1
 a=dim(C2)          
    a
 #forcer le statut matrice
Mat2=data.matrix(C2[1:15,2:16], rownames.force = T)     
row.names(Mat2)<-C2[1:15,1]   
  a
    
F2=graph.adjacency(Mat2,diag=FALSE,weighted=TRUE,mode="directed")
names.Vertex=rownames(Mat2)  
                
tkplot(F2, canvas.width=600, canvas.height=400,vertex.label=names.Vertex,
edge.width =( E(F2)$weight/2),edge.color="Steel Blue",vertex.size=4,vertex.color="Navy Blue",
vertex.label.color="Navy",edge.curved=TRUE, edge.arrow.size=1,5,tkplot.fit.to.screen)



