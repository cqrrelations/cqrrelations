#!/usr/bin/env python

# Copyright (c) 2008 Carnegie Mellon University.
#
# You may modify and redistribute this file under the same terms as
# the CMU Sphinx system.  See
# http://cmusphinx.sourceforge.net/html/LICENSE for more information.


import socket
UDP_IP = "127.0.0.1"
UDP_PORT = 5005

sock = socket.socket(socket.AF_INET, # Internet
#             socket.SOCK_STREAM) # TCP
             socket.SOCK_DGRAM) # UDP

import pygtk
pygtk.require('2.0')
import gtk

import gobject
import pygst
pygst.require('0.10')
gobject.threads_init()
import gst

class DemoApp(object):
    """GStreamer/PocketSphinx Demo Application"""
    def __init__(self):
        """Initialize a DemoApp object"""
        self.init_gst()

    def init_gst(self):
        """Initialize the speech components"""
        self.pipeline = gst.parse_launch('alsasrc ! capsfilter caps=audio/x-raw-int,rate=5000,channels=1 '
                                         + '! audioconvert ! audioresample ! vader name=vad auto-threshold=true '
                                         + '! pocketsphinx name=asr ! fakesink')

#        self.pipeline = gst.parse_launch('gconfaudiosrc ! audioconvert ! audioresample '
#                                         + '! vader name=vad auto-threshold=true '
#                                         + '! pocketsphinx name=asr ! fakesink')
        asr = self.pipeline.get_by_name('asr')
        asr.connect('partial_result', self.asr_partial_result)
        asr.connect('result', self.asr_result)
        asr.set_property('dict', '/home/mrybn/Documents/sphinx/pocketsphinx/model/lm/en_US/cmu07a.dic')
        asr.set_property('configured', True)

        bus = self.pipeline.get_bus()
        bus.add_signal_watch()
        bus.connect('message::application', self.application_message)

#        self.pipeline.set_state(gst.STATE_PAUSE)
        self.pipeline.set_state(gst.STATE_PLAYING)

    def asr_partial_result(self, asr, text, uttid):
        """Forward partial result signals on the bus to the main thread."""
        struct = gst.Structure('partial_result')
        struct.set_value('hyp', text)
        struct.set_value('uttid', uttid)
        asr.post_message(gst.message_new_application(asr, struct))

    def asr_result(self, asr, text, uttid):
        """Forward result signals on the bus to the main thread."""
        struct = gst.Structure('result')
        struct.set_value('hyp', text)
        struct.set_value('uttid', uttid)
        asr.post_message(gst.message_new_application(asr, struct))

    def application_message(self, bus, msg):
        """Receive application messages from the bus."""
        msgtype = msg.structure.get_name()
        if msgtype == 'partial_result':
            pass
          #  sock.sendto(bytes(msg.structure['hyp']+' '), (UDP_IP, UDP_PORT))
	
        elif msgtype == 'result':
#            self.pipeline.set_state(gst.STATE_PAUSE)
#            self.pipeline.set_state(gst.STATE_PLAYING)
            sock.sendto(bytes(msg.structure['hyp']+' '), (UDP_IP, UDP_PORT))

app = DemoApp()
gtk.main()
