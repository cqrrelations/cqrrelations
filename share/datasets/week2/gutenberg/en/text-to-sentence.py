 #!/usr/bin/env python

#first input is inputfile
#second outputfile
#thierd argument is author

import sys
from pattern.en import parsetree

fileinput = open(sys.argv[1])
fileoutput=fileinput.read().replace('K.', 'K')

theoutput = parsetree(fileoutput, tags = False, chunks = False)

from pattern.db import Datasheet
ds = Datasheet('sheet')

for sentence in theoutput:
	#ds.append(sentence)
	ds.append([sys.argv[3],sentence.string])

ds.save(sys.argv[2])


#w = csv.writer(file('trial.csv','wb'),quotechar='|')
