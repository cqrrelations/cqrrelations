# Demonstrator with Achmea Twitter data.
# Trains a machine learner on annotated insurance tweets,
# to predict whether a tweet is positive or negative,
# or a complaint or a question.

# Requires:
# - Python 2.7 : https://www.python.org/downloads
# - Pattern : http://www.clips.ua.ac.be/pattern

from pattern.db import Datasheet

from pattern.text import tokenize
from pattern.text import sentiment

from pattern.vector import count
from pattern.vector import chngrams
from pattern.vector import NB, SVM, SLP
from pattern.vector import kfoldcv

# -----------------------------------------------------------------------------

def vector(s):
    # A combination of techniques discussed in classification1.py.
    # Note the dictionary's update() method.
    # It takes another dictionary and adds its keys/values.
    polarity, subjectivity = sentiment(s, language="nl")
    v = {}
    s = tokenize(s)
    s = "\n".join(s)
    v.update(chngrams(s.lower(), n=2))
    v.update(chngrams(s.lower(), n=3))
    v.update(chngrams(s.lower(), n=4))
    v.update(count(s.lower().split(" ")))
    v["<polarity>"] = polarity + 1 # non-negative number
    v["<subjectivity>"] = subjectivity
    return v

# -----------------------------------------------------------------------------
# Load the data exported from Excel.
# Select the tweet text in each row, and possible classification labels.

data = []
files1 = ['csv/kafka.csv']
files2 = ['csv/holmes.csv']
files3 = ['csv/eliot.csv']
files4 = ['csv/weldon-johnson.csv']

files = files1 + files3 + files4 # change here
#files = files1
#files = files2
#files = files3
#files = files4

annotations = {}
ds0 = []
ds1 = []
ds2 = []
ds3 = []

for file in files:
    sheet = Datasheet.load(file)
    for row in sheet:
        if row[0] != '':
            tweet = row[1]
            author  = row[0] # "Positief", "Negatief" or "Neutraal"
            data.append([vector(tweet), author]) 
    # Also try predicting complaint or question instead of polarity.

# -----------------------------------------------------------------------------

distribution = {}
for tweet, label in data:
    if label not in distribution:
        distribution[label] = 0
    distribution[label] += 1

print distribution
print
# The class distribution is skewed,
# so the classifier may be biased.

# -----------------------------------------------------------------------------

# If you always use the same 500 vectors for testing, and the other 1500 
# for training, you are just testing how those 1500 predict the other 500. 

# To get a more reliable indication of how the machine learner will perform 
# in real-life, you can do multiple tests with different subsets of 
# test and training data. 

# This is called cross-validation. 
# Usually, the data set is chopped into 10 parts (or "folds"), 
# which are then rotated in 10 separate accuracy tests.

# The kfoldcv() function in the pattern.vector module does cross-validation.
# It takes a classifier and a list of (vector, label) data, and returns
# [accuracy, precision, recall, F1-score, standard deviation]:

[accuracy, precision, recall, F1score, deviation] = kfoldcv(NB, data, folds=10)
print "Accuracy: "+str(accuracy)
print "Precision: "+str(precision)
print "Recall: "+str(recall)
print "F-score: "+str(F1score)


# If you get an SVM error, this may mean that Pattern's pre-compiled
# SVM C-library doesn't work on your system. You may need to compile
# your own C-library from source. If you use the Sublime editor
# on Windows 7 and get an "libiomp5md.dll" error, 
# try using the excellent Spyder editor instead:
# https://code.google.com/p/spyderlib/

# Alternatively, you could train an SLP.
# The Single-Layered Perceptron is the simplest neural network.
# It is slower than SVM during training, but its accuracy is 
# usually comparable (1-2% less than SVM).

#print kfoldcv(SLP, data, folds=10, iterations=4)

# Don't worry too much whether you will be using SVM or Perceptron or other.
# You might see differences between them around 1-3% accuracy. 
# This might be for a number of reasons. 
# Keep in mind that accuracy scores are just a rough indicator, 
# a snapshot of reality, since they depend entirely on the (small) amount 
# of data you are using for training and testing.
# It is better to focus on good features and cross-validation.

# -----------------------------------------------------------------------------

# When you are satisfied with a classifier's performance,
# you can train and save it for deployment:

# Train & save:
# classifier = SVM(data)
# classifier.save("twitter-polarity.svm", final=True)

# # Load & deploy:
# classifier = SVM.load("twitter-polarity.svm")
# print classifier.classify(vector("Mooi gedaan!"))
# print classifier.classify(vector("Werkt belachelijk slecht..."))
