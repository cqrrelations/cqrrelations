
from lxml import html
from time import sleep
import codecs
import os

import mechanize
from mechanize import Browser 

#collect patent full text on lens.org
#first get links from search results (7 pages)
#search2: 'potato cultivar' in full text + A01H5/00 in classification -> 309 results on 7 pages

br = Browser()
#br.set_handle_robots(False)

counter=0

for i in range(7):
   searchresults_file=open('./potato_patents/lenssearchresults2/lenssearch2_'+str(i), 'r')
   rawsearchresults=searchresults_file.read()
   searchresults_file.close()
   doc = html.fromstring(rawsearchresults)

   rawlinks=doc.find_class("link fullText")
   for raw in rawlinks:
      if counter>=0: #change number to number at time-out
         link=raw.get('href')
         url="http://www.lens.org" + link + "/fulltext"
         name=link.split('/')[-1]
         print str(counter) + " : " + name

         patentpage=br.open(url).read()
         patentfile=open('./potato_patents/lens_patents/' + name, 'w')
         patentfile.write(patentpage)
         patentfile.close()
         sleep(10)
      counter+=1


