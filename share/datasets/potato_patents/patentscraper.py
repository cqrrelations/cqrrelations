#scraper for worldwide.espacenet.com/searchResults (European Patent Office)
#first save html of result page, and include path to file in source

#still some mistakes
#add tor to avoid anti-scraper behaviour

import requests
from lxml import html, etree
from time import sleep
import codecs


source="./cqrrelations_potato/Espacenet_results_total.html" #path to saved page of search results
sourcefile=open(source, 'r')
rawsource=sourcefile.read()
sourcefile.close()
doc = html.fromstring(rawsource)

rawlinks=doc.find_class('contentRowClass')

for raw in rawlinks:
   elements = raw.findall('td')
   patentid=elements[2].text_content().strip()
   CC=patentid[0:2]
   NRraw1=patentid[2:]
   NRraw2=NRraw1.split(u'\xa0')
   KCraw=NRraw2[1].split('(')
   KCraw2=KCraw[1].split(')')
   KC=KCraw2[0][0:2]
   print KC
   NR=NRraw2[0] + KC
   date=elements[3].text_content().strip().replace('-','')

   #url_description="http://worldwide.espacenet.com/publicationDetails/description?CC=" + CC + "&NR=" + NR + "&KC=" + KC + "&FT=D&ND=3&date=" + date + "&DB=EPODOC&locale=en_EP"
   #url_claims="http://worldwide.espacenet.com/publicationDetails/claims?CC=" + CC + "&NR=" + NR + "&KC=" + KC + "&FT=D&ND=3&date=" + date + "&DB=EPODOC&locale=en_EP"


   url_description="http://worldwide.espacenet.com/publicationDetails/description"
   url_claims="http://worldwide.espacenet.com/publicationDetails/claims"
   payload = {'CC':CC, 'NR':NR, 'KC':KC, 'FT':'D', 'ND':'3', 'date':date, 'DB':'EPODOC', 'locale':'en_EP'}
   page_description=requests.get(url_description, params=payload)
   page_claims=requests.get(url_claims, params=payload)

   description_file=codecs.open('./cqrrelations_potato/results/description_'+ NR, 'w', 'utf-8')
   description_file.write(page_description.text)
   description_file.close()
   claims_file=codecs.open('./cqrrelations_potato/results/claims_'+ NR, 'w', 'utf-8')
   claims_file.write(page_claims.text)
   claims_file.close()


