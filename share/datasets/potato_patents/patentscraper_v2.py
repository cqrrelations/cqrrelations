#scraper for worldwide.espacenet.com/searchResults (European Patent Office)
#first save html of result page, and include path to file in source

#still some mistakes
#add tor to avoid anti-scraper behaviour

import requests
from lxml import html, etree
from time import sleep
import codecs
import os

import mechanize
from mechanize import Browser 

 
source="./potato_patents/Espacenet_results_total.html" #path to saved page of search results
sourcefile=open(source, 'r')
rawsource=sourcefile.read()
sourcefile.close()
doc = html.fromstring(rawsource)
counter=0
br = Browser()
br.set_handle_robots(False)
init = br.open('http://worldwide.espacenet.com/searchResults?AB=&AP=&CPC=&DB=EPODOC&IC=A01H5&IN=&PA=&PD=&PN=&PR=&ST=advanced&Submit=Search&TI=potato&compact=true&locale=en_EP').read()

rawlinks=doc.find_class('contentRowClass')

for raw in rawlinks:
   elements = raw.findall('td')
   patentid=elements[2].text_content().strip()
   CC=patentid[0:2]
   NRraw1=patentid[2:]
   NRraw2=NRraw1.split(u'\xa0')
   KCraw=NRraw2[1].split('(')
   KCraw2=KCraw[1].split(')')
   KC=KCraw2[0][0:2]
   NR=NRraw2[0] + KC
   date=elements[3].text_content().strip().replace('-','')

   url_description="http://worldwide.espacenet.com/publicationDetails/description?CC=" + CC + "&NR=" + NR + "&KC=" + KC + "&FT=D&ND=3&date=" + date + "&DB=EPODOC&locale=en_EP"
   url_claims="http://worldwide.espacenet.com/publicationDetails/claims?CC=" + CC + "&NR=" + NR + "&KC=" + KC + "&FT=D&ND=3&date=" + date + "&DB=EPODOC&locale=en_EP"

   page_description=br.open(url_description).read()
   page_claims=br.open(url_claims).read()


   counter+=2
   if counter > 30 :
      os.system('printf "AUTHENTICATE \"DIOCANE\"\r\nSIGNAL NEWNYM\r\n" | nc 127.0.0.1 9051 -w 1') 	# this sets a nex exit node on the tor socket. it supposes you have DIOCANE as password in /etc/tor/torrc
      counter=0
      init = br.open('http://worldwide.espacenet.com/searchResults?AB=&AP=&CPC=&DB=EPODOC&IC=A01H5&IN=&PA=&PD=&PN=&PR=&ST=advanced&Submit=Search&TI=potato&compact=true&locale=en_EP').read()
      
   description_file=open('./potato_patents/results/description_'+ NR, 'w')
   description_file.write(page_description)
   description_file.close()
   claims_file=open('./potato_patents/results/claims_'+ NR, 'w')
   claims_file.write(page_claims)
   claims_file.close()


