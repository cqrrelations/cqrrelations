from pattern.db import Datasheet
from pattern.en import parse
from pattern.vector import Document, KNN
from sys import argv
from os import path
import glob

data = Datasheet.load(argv[1])

classifier = KNN()

i=0

for row in data.rows:
	line = row[0]
	score = int(row[1])
	i+=1

	classifier.train(Document(
		parse(
			line,
			tokenize=True,
			lemmata=True,
			tags=False,
			relations=True,
			chunks=False
		),
		type=score,
		stopwords=False
	))
	
	print "{0}: {1} features".format(i, len(classifier.features))

print classifier.features

classifier.finalize
classifier.save('classifier.inhi')
