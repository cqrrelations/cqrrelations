[d3.js][d3] is...
==============

<script src="d3.min.js"></script>

* short for data-driven documents (in javascript).
* a library that allows designers and developer to create visualisations of data in a web browser.
* a project led by [Mike Bostock](http://bost.ocks.org/mike/) (creates visualisations for nytimes.com)
* [BSD-licensed](http://opensource.org/licenses/BSD-3-Clause) open source and hosted on [github](https://github.com/mbostock/d3)
* a continuation of the earlier [Protovis](http://mbostock.github.io/protovis/) project, from the [Stanford Visualisation Group](http://vis.stanford.edu/)
* a "declarative" style programming that supports drawing by manipulating the structure of a document ([HTML][HTML] page and/or [SVG][SVG] elements).
* quite good at supporting interactive and live/changing data via transitions and the subtle notion of a [data join][join].
* concise.


[d3.js][d3] is not...
=====================

* [SVG][SVG]-specific (though it fits well together, and there are useful utility functions specific to [SVG][SVG])
* fixed to only producing a limited set of visualisations (though there is a library of "layouts" that implement a variety of popular visualisation techniques such as a [treemap][treemap] or [vornoi diagram][vornoi]).
* particularly suited to drawing in an imperative style (for instance on an [HTML][html] [canvas][canvas] element).


Selections
=============

Like [jquery][jquery], [d3][d3] uses selections. Instead of loops, selections perform actions to all elements that match.
```html
<script>
d3.selectAll("a").style("color", "pink");
</script>
```

It supports kind of [CSS][CSS] selector (like this one for paragraphs inside divs with class article), note the convention of *chaining* (the chain ends with a ';').
```html
<script>
d3.selectAll("div.article p")
    .style("border-left", "2px solid red")
    .attr("class", "special");
</script>
```

Page Manipulation
====================

All of the following can be applied to a selection to alter all of the selected elements.

* [attr](https://github.com/mbostock/d3/wiki/Selections#attr): set an element attribute
* [style](https://github.com/mbostock/d3/wiki/Selections#style): Set a CSS property in the element's style attribute
* [text](https://github.com/mbostock/d3/wiki/Selections#text), [html](https://github.com/mbostock/d3/wiki/Selections#html): Set the contents of an element
* [append](https://github.com/mbostock/d3/wiki/Selections#append): Add a new element
* [classed](https://github.com/mbostock/d3/wiki/Selections#classed): Add or remove a class based on a predicate (true-false) function
* ([property](https://github.com/mbostock/d3/wiki/Selections#property): Useful for getting / setting "values" of a form element)


Chain.Chain.Chain
==================

Old-school...
```javascript
x = d3.selectAll("div.article p");
y = x.style("border-left", "2px solid red");
z = y.attr("class", "special");
```

Pipeline style "chaining"...
```javascript
d3.selectAll("div.article p")
    .style("border-left", "2px solid red")
    .attr("class", "special");
```

Chaining works because most selection functions return the (same) selection object; note however that some commands (like *append*) change the selection; the convention is to shift the indention to indicate this. It's essential that the semi-color (;) only appears at the end of the chain (otherwise any dot (.) that follows is a syntax error).

```javascript
d3.selectAll("body")
    .style("background", "gray")
  .append("div")
    .text("Hello");
```


Selections + Dynamic properties
=============

[Random](http://en.wikipedia.org/wiki/Daniel_Bernoulli) [link](http://www.xanadu.com/) [colors](http://www.blairmag.com/)

```html
<script>
d3.selectAll("a")
    .style("color", function () {
        return d3.hsl(Math.random()*360, 1.0, 0.5);        
    });
</script>
```

or using i (index) to alternately style (even/odd):

```javascript
<script>
d3.selectAll("p")
    .style("background", function (d, i) {
        return i % 2 ? "#AAAAAA" : "white";        
    });
</script>
```

Selection + [Data][data]
==================
<div id="slide_01">
<ol>
<li>data</li>
<li>driven</li>
<li>documents</li>
</ol>
</div>

```javascript
    d3.selectAll("li")
        .data([12, 24, 48])
        .style("font-size", function (d) {
            return d + "px";
        });
```
<button id="data_button">run</button>

<script>
d3.select("#data_button").on("click", function () {
    d3.selectAll("#slide_01 li")
        .data([12, 24, 48])
        .style("font-size", function (d) {
            return d + "px";
        });
        d3.event.stopPropagation();
});
</script>


Structured data
================
<div id="slide_02">
<ol>
<li>data</li>
<li>driven</li>
<li>documents</li>
</ol>
</div>

```javascript
d3.selectAll("li")
    .data([
        {size: 12, color: "red"},
        {size: 24, color: "green"},
        {size: 48, color: "blue"}
    ])
    .style("font-size", function (d) {
        return d.size + "px";
    })
    .style("color", function (d) {
        return d.color + "px";
    });
```
<button id="data2_button">run</button>

<script>
d3.select("#data2_button").on("click", function () {
    d3.selectAll("#slide_02 li")
        .data([
            {size: 12, color: "red"},
            {size: 24, color: "green"},
            {size: 48, color: "blue"}
        ])
        .style("font-size", function (d) {
            return d.size + "px";
        })
        .style("color", function (d) {
            return d.color;
        });
        d3.event.stopPropagation();
});
</script>

Selection + Data = Join
=======================

d3 is said to be "data-driven" because you start with data, and then attach a kind of description of how the data should be represented which is then performed by the library. A "[Join][join]" is a special selection that triggers elements to be created for each element in a data set (list).

![](BostockJoin.png)

Image source: [Thinking with joins](http://bost.ocks.org/mike/join/)

```javascript
var update = d3.selectAll( ... )
    .data( [...] )
    ...;

update.enter()... ;

update.exit()... ;

```

Selection + Data = Join
=======================

<div id="colorwords" style="max-width: 50%; float: left"></div>

```javascript
function times (text, n) {
    var ret = '';
    for (var i=0; i<n; i++) { ret += text; }
    return ret;
}

d3.select("#colorwords")
    .selectAll("p")
    .data([3, 8, 7, 24, 36])
    .enter()
    .append("p")
    .text(function (d) {
        return times("hello ", d)
    })
    .style("color", function () {
        return d3.hsl(Math.random()*360, 1.0, 0.5);        
    });
```

<button class="colorwords">run</button>

<script>
(function () {

function times (text, n) {
    var ret = '';
    for (var i=0; i<n; i++) { ret += text; }
    return ret;
}

d3.select("button.colorwords").on("click", function () {
    var data = [3, 8, 7, 24, 36];
    d3.select("#colorwords")
        .selectAll("p")
        .data(data)
        .enter()
        .append("p")
        .text(function (d) {
            return times("hello ", d)
        })
        .style("color", function () {
            return d3.hsl(Math.random()*360, 1.0, 0.5);        
        });

    d3.event.stopPropagation();
})

})();
</script>

* [Join][join]
* [Object Constancy][constancy]

Data sources
=============
Provides handy functions to (asyncronously) load data from:

* [JSON](https://github.com/mbostock/d3/wiki/Requests#d3_json)
* [CSV](https://github.com/mbostock/d3/wiki/CSV) / [TSV](https://github.com/mbostock/d3/wiki/CSV#tsv)
* [Plain text](https://github.com/mbostock/d3/wiki/Requests#d3_text)

Let's Make a Bar Chart
======================

The [d3][d3] site / Mike Bostock's own blog post [tutorials](https://github.com/mbostock/d3/wiki/Tutorials) are very good introductions to the core concepts of [d3][d3].

* [Let's make a bar chart](http://bost.ocks.org/mike/bar/) (parts 1, 2, 3)
* [Three little circles](http://bost.ocks.org/mike/circles/)
* [Gallery of examples](https://github.com/mbostock/d3/wiki/Gallery)


Scales
====================

* [Linear](https://github.com/mbostock/d3/wiki/Quantitative-Scales#linear)
* [Logarithmic](https://github.com/mbostock/d3/wiki/Quantitative-Scales#log)

```javascript
function timeline () {
    var timescale = d3.scale.linear()
        .domain([start, end])
        .range([0, timeline_right]);

    this        
        .style("left", function (d) {
            return timescale(d['date']) +"px";
        })
        .style("top", 100+"px");
}
```

Layouts
====================

Many [layouts](https://github.com/mbostock/d3/wiki/Layouts) are packaged with d3 which are laterally integrated -- they often work as data filters (take some data input and produce data lists suitable for use with the *usual* d3 [data][data] function). Other utilities offer for support for drawing geographic data.

* [trees](https://github.com/mbostock/d3/wiki/Tree-Layout) & [treemap][treemap]
* [force](https://github.com/mbostock/d3/wiki/Force-Layout) (network / graph)
* [geo](https://github.com/mbostock/d3/wiki/Geo-Paths)

I've been using d3 in different projects...
====================

with [Nicolas Malevé](http://www.constantvzw.org/site/Constant-members.html) and [Ellef Prestsæter](http://www.kunstkritikk.com/skribenter/ellef-prestsaeter/):

* [SISSV: filing cabinet](http://sissv.activearchives.org/cabinet/) 
* [SISSV: SIFT feature browser](http://sissv.activearchives.org/features/05_collage.html)
* [Guttorm Guttormsgaard: Eleven orderings](http://guttormsgaard.activearchives.org/)

with [Femke Snelting](http://snelting.domainepublic.net/):

* [Who's who](http://whoswho.vti.be/) (Network &amp; Treemap)

## other links

* [Top disproportionately common names by profession](http://verdantlabs.com/professions/index.html) (echo of classic info vis: [Martin Wattenberg's Baby Name Wizard](http://www.babynamewizard.com/voyager)
from [collection](http://www.cc.gatech.edu/gvu/ii/resources/infovis.html) of information visualisation)
* [http://benfry.com/zipdecode/](http://benfry.com/zipdecode/) Ben Fry: Zip Decoder

<script>
d3.selectAll("a")
    .style("color", function () {
        return d3.hsl(Math.random()*360, 1.0, 0.5);        
    });
</script>

[d3]: http://d3js.org/ "Main d3 website"
[svg]: https://developer.mozilla.org/en/docs/Web/SVG "Mozilla Resources about SVG"
[html]: https://developer.mozilla.org/en-US/docs/Glossary/HTML "Mozilla Glossary Entry on HTML"
[join]: http://bl.ocks.org/mbostock/3808234
[constancy]: http://bost.ocks.org/mike/constancy/ "d3 Tutorial about Object Constancy"
[data]: https://github.com/mbostock/d3/wiki/Selections#data "d3 API Documentation on the data function"
[treemap]: https://github.com/mbostock/d3/wiki/Treemap-Layout
[vornoi]: https://github.com/mbostock/d3/wiki/Voronoi-Geom
[jquery]: http://jquery.org "The very popular javascript helper library jQuery"
[canvas]: https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API "HTML5 canvas element resources on Mozilla"
[css]: https://developer.mozilla.org/en-US/docs/Web/CSS "CSS resources on Mozilla"


