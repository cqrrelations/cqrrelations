import html5lib, sys, os
from xml.etree import ElementTree as ET 
p = sys.argv[1]

with open(p) as f:
    t = html5lib.parse(f, namespaceHTMLElements = False)

    for item in t.findall(".//link"):
        if item.get("href", "").endswith("slidy.css"):
            item.set("href", "slidy.css")
    for item in t.findall(".//script"):
        if item.get("src", "").endswith("slidy.js"):
            item.set("src", "slidy.js")

with open ("tmp.html", "w") as f:
    print >> f, ET.tostring(t, method="html")
os.rename("tmp.html", p)

