# Open Street Map

Presentation by Nicolas Malevé

Thursday January 15, 2015

* [OpenStreetMap](http://www.openstreetmap.org/)
* [OpenStreetMap | Public GPS traces](http://www.openstreetmap.org/traces)
* [OpenStreetMap | Users' diaries](https://www.openstreetmap.org/diary)
* [OpenStreetMap | Polyglot's diary | Mapping public transport in Belgium](https://www.openstreetmap.org/user/Polyglot/diary/28401)
* [OpenStreetMap](https://www.openstreetmap.org/copyright)

* [GPSBabel development:Chapter 3. The Formats](http://www.gpsbabel.org/htmldoc-1.5.2/The_Formats.html)
* [GeoNames](http://www.geonames.org/)
* [OpenStreetMap Nominatim: Search](http://nominatim.openstreetmap.org/)

* [Welcome to the QGIS project!](http://www.qgis.org)

* [Stamen, Compositing, and Mapnik 2.1](http://mapnik.org/news/2012/08/27/stamen-compositing-mapnik-v2.1/)
* [stamen design | Trees, Cabs & Crime in the Venice Biennale](http://content.stamen.com/trees-cabs-crime_in_venice)
* [maps.stamen.com](http://maps.stamen.com/#watercolor/12/37.7706/-122.3782)
* [Map Stack by Stamen](http://mapstack.stamen.com)

* [OpenLayers 3 - Welcome](http://openlayers.org/)
* [Leaflet - a JavaScript library for mobile-friendly maps](http://leafletjs.com/)

* [Open Street Map Philippines - Mapping the Philippines](http://openstreetmap.org.ph/)
* [Encuentros cartogrÃ¡ficos - Logbook](http://encuentros-cartograficos.net/logbook/spip.php?page=comparacion&id_mot=9)

* [walking papers (tecznotes)](http://mike.teczno.com/notes/walking-papers.html)

* [bMa - Bouwmeester Maître Architecte](http://www.bmabru.be)
* [buratinas](http://buratinas.nadine.be/)
* [Vous êtes là](http://tram81.be/)
