from urllib2 import urlopen
from html5lib import parse

with open("links_nicolas.txt") as f:
    for line in f:
        line = line.strip()
        if line:
            url = line
            f = urlopen(url)
            t = parse(f, namespaceHTMLElements=False)
            title = t.find(".//title")
            print u"* [{0}]({1})".format(title.text, url).encode("utf-8")

        else:
            print