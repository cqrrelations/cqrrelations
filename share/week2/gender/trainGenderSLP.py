from pattern.web import Element
from pattern.en import parse
from pattern.vector import Document, SLP
from sys import argv
from os import path
import glob

print argv[1]
searchpath = path.join(argv[1], "*.xml")

genderClassifier = SLP()

print "Looking for {0}".format(searchpath)

counter = 0

for filepath in glob.glob(searchpath):
	filename = path.split(filepath)[1]
	props = filename.split('.')
	gender = props[1]
	
	f = open(filepath, 'r')
	html = f.read()
	f.close()
	elements = Element(html)
	
	for post in elements('post'):
		text = post.content.strip()
		
		v = Document(
				parse(
					text,
					tokenize=True,
					lemmata=True,
					tags=False,
					relations=False,
					chunks=False
				),
				type=gender,
				stopwords=False
			)
		
		genderClassifier.train(v)

	counter += 1
	
	print '.'
	
	if counter % 50 == 0:
		genderClassifier.save('./SLPResults.ihni')
		
		print "Learned about {0} authors".format(counter)
	