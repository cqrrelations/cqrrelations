#!/usr/bin/env python
from pattern.web import plaintext
from pattern.vector import Document, NB, KNN, SLP, POLYNOMIAL, LEMMA
from pattern.db import csv

# classifier = SVM(kernel=POLYNOMIAL, degree=10)
classifier = KNN()

# test a custom tagger
def instance(review):                     # "Great book!"
	from pattern.nl import tag, predicative, parsetree
	from pattern.vector import count
	v = tag(review)                       # [("Great", "JJ"), ("book", "NN"), ("!", "!")]
	# print v
	v = [word for (word, pos) in v if pos in ("JJ", "RB", "CC", "IN", "DT", "MD", "PRP") or word in ("!")]
	# v = [word for (word, pos) in v]
	# v = [parsetree(word, relations=True, lemmata=True)[0].lemma[0] for (word,pos) in v]
	# print v
	v = [predicative(word) for word in v] # ["great", "!", "!"]
	v = count(v)                          # {"great": 1, "!": 1}
	return v

print 'TRAINING:'
for title, content, newspaper in csv('newspaper_items.csv'):
	content = title + '\n ' + content
	v = Document(content, type=newspaper, stopwords=False, stemmer=LEMMA)
	# v = instance(content)
	classifier.train(v, newspaper)
	print newspaper, title

print 'CLASSES:',classifier.classes
print 'terms in classifier:', len(classifier.terms)

classifier.save("newspapers.p")
print 'Saved classifier'
