Portable Cqrrelations Archive 2015
==================================


The pages of this (local) archive come from one of two sources:

* the local etherpad
* directy in the git repository

To edit pages via the etherpad
------------------------------

* Go to the [local etherpad](http://10.1.10.1:9001/p/start) start page.
* Use double-brackets to link to new pages.
* Pages linked from the start page are published nightly to [the git repository](/etherpad_archive/)

To add files and other materials to the [share](http://10.1.10.1/share) folder:
-------------------------------------------------------------------------------

* Connect via SFTP to [ftp://10.1.10.1](ftp://10.1.10.1) with *cqrrelations* as user and password.
* If you want to add very big files (>100Mb), or files you don't want to end up in a public repository, put them in the [tmp](http://10.1.10.1/tmp) folder, instead.

Links
-----------------

* [published version of the etherpad](http://10.1.10.1/etherpad_archive)
* [local etherpad](http://10.1.10.1:9001/p/start) (on port 9001)
* [Cqrrelations e-book Library](/share/texts)


*****
*****
*****


A FEMINIST NET/WORK HOW-TO
-----------------------------

* Setup local server(s) & make our own network infrastructure, rather than "fixing the problems of internet access" by paying for increased bandwidth, 
* Prefer "read/write" networks to those that "just work" ,
* Prefer pocket servers to those in the clouds,
* Embrace a diversity of network topologies and different scales (machine to machine, local nodes, institutional infrastructure) and consider the implications of working with each,
 rather than a Web 2.0 model where resources must be uploaded onto "the 24/7 Internet (tm)" in order to share them,
* Invite users to look critically at the implications of *any* infrastructural decisions, rather than imagining utopic and/or "killer" solutions, 
* Make that which is normally hidden and invisible (tending to surveillance), explicit and shared (as a gesture of collective authorship), for instance:
    instead of caching web resources (silently), we imagine services to archive pages and share them locally as networked cookbooks,
    rather than logging IRC conversations on a server or database accessible only by administrators, we imagine (local) logs available for reading / editing by participants and published conditionally.

The provided network is a starting point; the network topology can be tweaked, changed, extended. 
The user and password to access via ssh is cqrrelations / cqrrelations.


